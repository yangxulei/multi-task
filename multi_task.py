import os
import math
import numpy as np
import skimage.io as skio
import skimage.transform as skt
import scipy.ndimage as ndi

from pandas import DataFrame
from pandas.io.parsers import read_csv

from keras import backend as K
from keras.engine.training import Model as KerasModel
from keras.layers import Input, Dense, Activation, Flatten, Dropout, merge
from keras.layers import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator, Iterator, array_to_img, transform_matrix_offset_center, flip_axis, random_channel_shift#, apply_transform
from keras.callbacks import Callback, EarlyStopping
from keras.models import load_model


INPUT_HEIGHT = 128
INPUT_WIDTH = 128
LR = 0.001
EPOCHS = 10000
BATCH_SIZE = 32

NORMALIZE = False

def preprocess_input(x):
    x /= 255.
    x -= 0.5
    x *= 2.
    return x


def apply_transform(x, transform_matrix, channel_index=0, fill_mode='nearest', cval=0.):
    x = np.rollaxis(x, channel_index, 0)
    final_affine_matrix = transform_matrix[:2, :2]
    final_offset = transform_matrix[:2, 2]
    channel_images = [ndi.interpolation.affine_transform(x_channel, final_affine_matrix,
                      final_offset, order=3, mode=fill_mode, cval=cval) for x_channel in x]
    x = np.stack(channel_images, axis=0)
    x = np.rollaxis(x, 0, channel_index+1)
    return x


class LRDecay(Callback):
    def __init__(self, start=0.001, stop=0.0001, max_epochs=200):
        super(LRDecay, self).__init__()
        self.start, self.stop = start, stop
        self.ls = np.linspace(self.start, self.stop, max_epochs)

    def on_epoch_begin(self, epoch, logs={}):
        new_value = self.ls[epoch]
        K.set_value(self.model.optimizer.lr, new_value)


class CheckpointCallback(Callback):
    def __init__(self, start_index, save_periodic=True, period=10000):
        super(CheckpointCallback, self).__init__()
        self.start_index = start_index
        self.save_periodic = save_periodic
        self.period = period

    def on_epoch_end(self, epoch, logs={}):
        if self.save_periodic:
            if (self.start_index + epoch) % self.period == 0:
                fname = os.path.join('saved_3_{}.model'.format(self.start_index + epoch))
                self.model.save(fname)


class MyGenerator(ImageDataGenerator):
    def __init__(self,
                 rotation_range=0.,
                 width_shift_range=0.2,
                 height_shift_range=0.2):

        super(MyGenerator, self).__init__(rotation_range=rotation_range,
                                          width_shift_range=width_shift_range,
                                          height_shift_range=height_shift_range,
                                          fill_mode='constant',
                                          cval=0.,
                                          dim_ordering='th')

    # override to return params
    def my_random_transform(self, x):
        # x is a single image, so it doesn't have image number at index 0
        img_row_index = self.row_index - 1
        img_col_index = self.col_index - 1
        img_channel_index = self.channel_index - 1

        # use composition of homographies to generate final transform that needs to be applied
        if self.rotation_range:
            theta = np.pi / 180 * np.random.uniform(-self.rotation_range, self.rotation_range) #can do something here to shift the skewed distributions of the angle of GE images
        else:
            theta = 0
        rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                    [np.sin(theta), np.cos(theta), 0],
                                    [0, 0, 1]])
        mat_rotation = np.array([[np.cos(theta), np.sin(theta), 0],
                                 [-np.sin(theta), np.cos(theta), 0],
                                 [0, 0, 1]])
        if self.height_shift_range:
            tx = np.random.uniform(-self.height_shift_range, self.height_shift_range) * x.shape[img_row_index]
        else:
            tx = 0

        if self.width_shift_range:
            ty = np.random.uniform(-self.width_shift_range, self.width_shift_range) * x.shape[img_col_index]
        else:
            ty = 0

        # tx is height, ty is width
        translation_matrix = np.array([[1, 0, tx],
                                       [0, 1, ty],
                                       [0, 0, 1]])
        mat_translation = np.array([[1, 0, -tx],
                                    [0, 1, -ty],
                                    [0, 0, 1]])
        if self.shear_range:
            raise RuntimeError('not implemented')
            shear = np.random.uniform(-self.shear_range, self.shear_range)
        else:
            shear = 0
        shear_matrix = np.array([[1, -np.sin(shear), 0],
                                 [0, np.cos(shear), 0],
                                 [0, 0, 1]])


        if self.zoom_range[0] == 1 and self.zoom_range[1] == 1:
            zx, zy = 1, 1
        else:
            raise RuntimeError('not implemented')
            zx, zy = np.random.uniform(self.zoom_range[0], self.zoom_range[1], 2)
        zoom_matrix = np.array([[zx, 0, 0],
                                [0, zy, 0],
                                [0, 0, 1]])

        transform_matrix = np.dot(np.dot(np.dot(rotation_matrix, translation_matrix), shear_matrix), zoom_matrix)
        mat_transform = np.dot(mat_translation, mat_rotation)
        
        h, w = x.shape[img_row_index], x.shape[img_col_index]
        transform_matrix = transform_matrix_offset_center(transform_matrix, h, w)
        mat_transform = transform_matrix_offset_center(mat_transform, h, w)
        x = apply_transform(x, transform_matrix, img_channel_index,
                            fill_mode=self.fill_mode, cval=self.cval)
        if self.channel_shift_range != 0:
            x = random_channel_shift(x, self.channel_shift_range, img_channel_index)

        if self.horizontal_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_col_index)

        if self.vertical_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_row_index)

        return x, mat_transform, tx, ty, theta


    def flow_from_imglist(self, X, y=None, 
                          target_size=(INPUT_HEIGHT, INPUT_WIDTH),
                          batch_size=32, shuffle=True, seed=None,
                          save_to_dir=None, save_prefix='', save_format='jpeg'):
        return ImgListIterator(X, y, self,
                               target_size=target_size,
                               batch_size=batch_size, shuffle=shuffle, seed=seed,
                               dim_ordering=self.dim_ordering,
                               save_to_dir=save_to_dir, save_prefix=save_prefix, save_format=save_format)


    
class ImgListIterator(Iterator):

    def __init__(self, X, y, image_data_generator,
                 target_size=(INPUT_HEIGHT, INPUT_WIDTH),
                 batch_size=32, shuffle=False, seed=None,
                 dim_ordering='default',
                 save_to_dir=None, save_prefix='', save_format='jpeg'):
        if y is not None and len(X) != len(y):
            raise Exception('X (images) and y (labels) '
                            'should have the same length. '
                            'Found: X : %s, y : %s' % (len(X), len(y)))
        if dim_ordering == 'default':
            dim_ordering = K.image_dim_ordering()
        self.dim_ordering = dim_ordering
        self.X = X
        self.y = y
        self.image_data_generator = image_data_generator
        self.target_size = tuple(target_size)
        if self.dim_ordering == 'tf':
            self.image_shape = self.target_size + (1,)
        else:
            self.image_shape = (1,) + self.target_size
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format
        super(ImgListIterator, self).__init__(len(X), batch_size, shuffle, seed)

    def next(self):
        # for python 2.x.
        # Keeps under lock only the mechanism which advances
        # the indexing of each batch
        # see http://anandology.com/blog/using-iterators-and-generators/
        with self.lock:
            index_array, current_index, current_batch_size = next(self.index_generator)
        # The transformation of images is not under thread lock so it can be done in parallel

        # default to th ordering
        batch_x = np.zeros((current_batch_size, 1,) + self.target_size)
        batch_y_a = np.zeros((current_batch_size,) + (2, ))
        batch_y_p = np.zeros((current_batch_size,) + (2, ))
        for i, j in enumerate(index_array):
            height, width = self.X[j].shape
            x = skt.resize(self.X[j], self.target_size)
            x = np.expand_dims(x, axis=0)
            x, transform_matrix, tx, ty, theta = self.image_data_generator.my_random_transform(x)
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x

            if self.y is not None:
                x1, y1 = self.y[j][0]
                x2, y2 = self.y[j][1]
                x1 = x1 * float(self.target_size[1])/width
                x2 = x2 * float(self.target_size[1])/width
                y1 = y1 * float(self.target_size[0])/height
                y2 = y2 * float(self.target_size[0])/height
                mat = np.array([[y1, y2], [x1, x2], [1, 1]])
                mat = np.dot(transform_matrix, mat)
                if not NORMALIZE:
                    x1 = mat[1, 0]
                    y1 = mat[0, 0]
                    x2 = mat[1, 1]
                    y2 = mat[0, 1]
                    batch_y_a[i, 0] = x2 - x1
                    batch_y_a[i, 1] = y2 - y1
                    batch_y_p[i, 0] = 0.5*(x1+x2)
                    batch_y_p[i, 1] = 0.5*(y1+y2)
                else:
                    batch_y[i, 0] = (mat[1, 0] - width/2)/(width/2)
                    batch_y[i, 1] = (mat[0, 0] - height/2)/(height/2)
                    batch_y[i, 2] = (mat[1, 1] - width/2)/(width/2)
                    batch_y[i, 3] = (mat[0, 1] - height/2)/(height/2)

        # save augmented images to disk for debugging purposes
        if self.save_to_dir:
            for i in range(current_batch_size):
                gray_img = batch_x[i, 0, :, :]
                img = array_to_img(gray_img, dim_ordering='th', scale=True)
                fname = '{prefix}_{index}_{hash}.{format}'.format(prefix=self.save_prefix,
                                                                  index=current_index + i,
                                                                  hash=np.random.randint(1e4),
                                                                  format=self.save_format)
                img.save(os.path.join(self.save_to_dir, fname))


        if self.dim_ordering == 'tf':
            batch_tmp = np.zeros((current_batch_size,) + self.target_size + (1,))
            for i in range(current_batch_size):
                batch_tmp[i] = np.transpose(batch_x[i, 0:1, :, :], (1, 2, 0))

            batch_x = batch_tmp
            
        if self.y is None:
            return {'input': batch_x}
        else:
            return {'input': batch_x}, {'outp_a': batch_y_a, 'outp_p': batch_y_p}



    
def load_data(data_dir, data_csv, load_pts=True):
    df = read_csv(data_csv)
    img_ids = df['ID']

    imgs = []
    for img_name in img_ids:
        img = skio.imread('%s/%s.tif' % (data_dir, img_name), as_grey=True)
        imgs.append(img)
        
    if load_pts:
        x1 = df['X1'].values
        y1 = df['Y1'].values
        x2 = df['X2'].values
        y2 = df['Y2'].values
    
        pts1 = np.array(zip(x1, y1))
        pts2 = np.array(zip(x2, y2))

    print 'Num of images: {}'.format(len(imgs))

    if load_pts:
        return img_ids, imgs, pts1, pts2
    else:
        return img_ids, imgs


def build_model():
    MOMENTUM = 0.5
    if K.image_dim_ordering() == 'th':
        inp = Input(shape=(1, INPUT_HEIGHT, INPUT_WIDTH), name='input')
    elif K.image_dim_ordering() == 'tf':
        inp = Input(shape=(INPUT_HEIGHT, INPUT_WIDTH, 1), name='input')
 
    x = Convolution2D(32, 3, 3, border_mode='same')(inp)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), border_mode='same')(x)
    x = Dropout(0.1)(x)
   
    x = Convolution2D(64, 3, 3, border_mode='same')(x)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), border_mode='same')(x)
    x = Dropout(0.2)(x)
 
    x = Convolution2D(96, 1, 1, border_mode='same')(x)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Convolution2D(128, 3, 3, border_mode='same')(x)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), border_mode='same')(x)
    shared = Dropout(0.3)(x)

    x = Convolution2D(192, 1, 1, border_mode='same')(shared)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Convolution2D(256, 3, 3, border_mode='same')(x)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), border_mode='same')(x)
    x = Dropout(0.3)(x)
 
    x = Flatten()(x)
 
    x = Dense(768)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
 
    x = Dense(512)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
    outp_angle = Dense(2, activation='linear', name = 'outp_a')(x)
    
    x = Convolution2D(192, 1, 1, border_mode='same')(shared)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Convolution2D(256, 3, 3, border_mode='same')(x)
    x = BatchNormalization(momentum=MOMENTUM)(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), border_mode='same')(x)
    x = Dropout(0.3)(x)
 
    x = Flatten()(x)
 
    x = Dense(768)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    x = Dense(512)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
    outp_point = Dense(2, activation='relu', name = 'outp_p')(x)
      
    model = KerasModel(input=inp, output=[outp_angle, outp_point])
 
    learning_method = Adam(lr=LR)
 
    model.compile(optimizer=learning_method,
          loss={'outp_a': cos_proximity, 'outp_p': 'mean_squared_error'},
          loss_weights={'outp_a': 0.5, 'outp_p': 0.5})
 
 
    print model.summary()
 
    return model


def cos_proximity(y_true, y_pred):
    y_true = K.l2_normalize(y_true, axis=-1)
    y_pred = K.l2_normalize(y_pred, axis=-1)
    return -K.mean(y_true * y_pred, axis=-1)



def train(model, imgs, pts1, pts2, start_index=0):
    y = zip(pts1, pts2)
    train_iter = create_generator(angle=30., shift=0.05).flow_from_imglist(imgs, y, target_size=(INPUT_HEIGHT, INPUT_WIDTH), batch_size=BATCH_SIZE, shuffle=True)
    
    model.fit_generator(train_iter,
                        samples_per_epoch=len(imgs),
                        nb_epoch=EPOCHS,
                        verbose=1,
                        callbacks=[CheckpointCallback(start_index), LRDecay(LR, LR/100, EPOCHS)])
    

def train_model(data_dir, data_csv, save_model_fname, prev_fname=None, start_index=0):
    _, imgs, pts1, pts2 = load_data(data_dir, data_csv)

    model = build_model()
    if prev_fname is not None:
        model.load_weights(prev_fname)
    train(model, imgs, pts1, pts2, start_index)
    model.save(save_model_fname)    



def display(data_dir, data_csv):
    img_ids, imgs, pts1, pts2 = load_data(data_dir, data_csv)

    for i, img_name in enumerate(img_ids):
        img = skio.imread('%s/%s.tif' % (data_dir, img_name), as_grey=True)
        print img_name
        if data_csv == 'pred.csv':
            show_img(img, pts1[i]*2, pts2[i]*2)
        else:
            show_img(img, pts1[i]*2, pts2[i]*2)
        
        
def show_img(img, pts1, pts2):
    import sys
    import matplotlib.pyplot as plt

    def press(event):
        if event.key == 'q':
            print 'Terminated by user'
            sys.exit()
        elif event.key == 'c':
            plt.close()


    x1, y1 = pts1
    x2, y2 = pts2

    plt.ioff()
    fig = plt.figure(frameon=False)
    fig.canvas.set_window_title('Image')
    fig.canvas.mpl_connect('key_press_event', press)
    ax = fig.add_subplot(1, 1, 1)
    ax.set_frame_on(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)
    ax.imshow(img, cmap='gray')
    ax.plot([x1, x2], [y1, y2], 'r-', lw=2)
    print x1, y1, x2, y2, (y2-y1)/(x2-x1), np.arctan2(y2-y1, x2-x1) * 180/math.pi
    plt.scatter([x1, x2], [y1, y2], s=20)
    plt.show()
    return np.arctan2(y2-y1, x2-x1) * 180/math.pi


def show_img_4(img, pts1, pts2, gt1, gt2, title='Image'):
    import sys
    import matplotlib.pyplot as plt

    def press(event):
        if event.key == 'q':
            print 'Terminated by user'
            sys.exit()
        elif event.key == 'c':
            plt.close()


    x1, y1 = pts1
    x2, y2 = pts2

    gx1, gy1 = gt1
    gx2, gy2 = gt2

    diff = np.arctan2(y2-y1, x2-x1) * 180/math.pi - np.arctan2(gy2-gy1, gx2-gx1) * 180/math.pi

    plt.ioff()
    fig = plt.figure(frameon=False)
    fig.canvas.set_window_title(title)
    fig.canvas.mpl_connect('key_press_event', press)
    ax = fig.add_subplot(1, 1, 1)
    ax.set_frame_on(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)
    ax.imshow(img, cmap='gray', interpolation='bicubic')
    ax.plot([x1, x2], [y1, y2], 'r-', lw=1)
    print np.arctan2(y2-y1, x2-x1) * 180/math.pi, np.arctan2(gy2-gy1, gx2-gx1) * 180/math.pi
    plt.scatter([x1, x2], [y1, y2], s=20)
    ax.plot([gx1, gx2], [gy1, gy2], 'b-', lw=2) 
    plt.scatter([gx1, gx2], [gy1, gy2], s=20)
    plt.title(title+'_'+str(diff))
    img_name = os.path.join('./output',title + '.tif')
    plt.show()

    return np.arctan2(y2-y1, x2-x1) * 180/math.pi, np.arctan2(gy2-gy1, gx2-gx1) * 180/math.pi



        
def create_generator(angle=0., shift=0.):
    datagen = MyGenerator(rotation_range=angle,
                          width_shift_range=shift,
                          height_shift_range=shift)

    return datagen



def predict_iter(model_fname, data_dir, data_csv):
    img_ids, imgs, pts1, pts2 = load_data(data_dir, data_csv)
    
    import time
    start = time.time()
    
    y = zip(pts1, pts2)
    iter = create_generator().flow_from_imglist(imgs, y, target_size=(INPUT_HEIGHT, INPUT_WIDTH), batch_size=1, shuffle=False)

    model = load_model(model_fname, {'cos_proximity': cos_proximity})

    arrA1 = np.zeros(len(img_ids))
    arrA2 = np.zeros(len(img_ids))
    arrP = np.zeros(len(img_ids))

    for i, (X, y) in enumerate(iter):
        if i >= len(img_ids):
            break
        X = X['input']
        img = X[0, 0, :, :]
        target_height, target_width = img.shape
        print i, img_ids[i]
        p2, p1 = model.predict(X)
        if not NORMALIZE:
            dx = p2[0, 0]
            dy = p2[0, 1]
            norm = np.sqrt(dx*dx + dy*dy)
            dx = dx/norm
            dy = dy/norm
            xc, yc = p1[0, 0], p1[0, 1]

            pt1 = 0, np.clip(yc - xc*(dy/dx), 0, target_height)
            pt2 = 128, np.clip(yc + (128-xc)*(dy/dx), 0, target_height)
          
            px1, py1 = pt1
            px2, py2 = pt2
            arrA1[i] = np.arctan2(py2-py1, px2-px1) * 180/math.pi
            gx1, gy1 = pts1[i]
            gx2, gy2 = pts2[i]
            gx1 = gx1 * float(target_width)/imgs[0].shape[1]
            gx2 = gx2 * float(target_width)/imgs[0].shape[1]
            gy1 = gy1 * float(target_height)/imgs[0].shape[0]
            gy2 = gy2 * float(target_height)/imgs[0].shape[0]
            arrA2[i] = np.arctan2(gy2-gy1, gx2-gx1) * 180/math.pi
            arrP[i] = math.sqrt(pow(0.5*(gx1+gx2) - 0.5*(px1+px2), 2) + pow(0.5*(gy1+gy2) - 0.5*(py1+py2), 2))
            print arrA1[i], arrA2[i]
            show_img_4(img, pt1, pt2, (gx1, gy1), (gx2, gy2), img_ids[i])
        else:
            raise RuntimeError('obsolete')
            pt1 = p[0, 0]*width/2 + width/2, p[0, 1]*height/2 + height/2
            pt2 = p[0, 2]*width/2 + width/2, p[0, 3]*height/2 + height/2

    
    print arrA1-arrA2 
    print np.max(arrA1-arrA2), np.min(arrA1-arrA2), np.mean(abs(arrA1-arrA2)), np.std(abs(arrA1-arrA2))    
    print np.max(arrP), np.min(arrP), np.mean(abs(arrP)), np.std(abs(arrP))    

    end = time.time()
    print 'testing time for 30 images: ', (end - start)
            
def test_iter(data_dir, data_csv):
    img_ids, imgs, pts1, pts2 = load_data(data_dir, data_csv)
    y = zip(pts1, pts2)
    iter = create_generator(angle=30., shift=0.05).flow_from_imglist(imgs, y, target_size=(INPUT_HEIGHT, INPUT_WIDTH), batch_size=1, shuffle=False)

    for i, (X, y) in enumerate(iter):
        X = X['input']
        p2 = y['outp_a']
        p1 = y['outp_p']

        dx = p2[0, 0]
        dy = p2[0, 1]
        norm = np.sqrt(dx*dx + dy*dy)
        dx = dx/norm
        dy = dy/norm

        xc, yc = p1[0, 0], p1[0, 1]

        img = X[0, 0, :, :]
        target_height, target_width = img.shape

        pt1 = 0, np.clip(yc - xc*(dy/dx), 0, target_height)
        pt2 = 128, np.clip(yc + (128-xc)*(dy/dx), 0, target_height)

        show_img(img, pt1, pt2)


if __name__ == '__main__':
    import time
    start = time.time()
    #train_model('corpus', 'corpus/train.csv', 'final.model')
    predict_iter('./final.model', 'corpus', 'corpus/test.csv')
    
    end = time.time()
    print 'running time: ', (end - start)
