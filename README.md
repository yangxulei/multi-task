# multi-task deep learning model by yangxl@i2r.a-star.edu.sg 

This repository is to support our submission to Neurocomputing:

Title: Automatic detection of anatomical landmarks in brain MR scanning using multi-task deep neural networks

Notes: the image set involved in the manuscript is not available due to confidential issues, but the implementation 
	is general and can be used for all landmark detection applications.

How to Run:

	Training: in the training csv file, specify the path of each training image, as well as the coordinates of the two ground truth points
	
	Testing: in the testing csv file, specify the path of each testing image, the model will output location and angle 